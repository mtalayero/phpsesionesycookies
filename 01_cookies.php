<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	
	if (isset($_POST['nombre'])){
		setcookie('nombre', $_POST['nombre'], 0);
	}
?>
<DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<title>header</title>
		<link rel="stylesheet" href="css/style.css">		
	</head>
	<body>		
		<?php		
			if (isset($_COOKIE['nombre'])){
				echo "<p>Hola " .  $_COOKIE['nombre'] . "</p>";
			}
			echo (isset($_COOKIE['nombre']) ?
				"<p>Hola, " . $_COOKIE['nombre'] . "</p>":
				"<p>Hola Anonimo</p>");
		?>
		<form action="01_cookies.php">
			<input type="text" name="nombre"/>
			<input type="submit" value="Enviar"/>			
		</form>
	</body>
</html>