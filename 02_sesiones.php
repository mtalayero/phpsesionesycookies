<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	
	session_start();
	if (isset($_POST['nombre'])){
		$_SESSION['nombre'] = $_POST['nombre'];
		session_regenerate_id(); //regenerar un id al hacer algun cambio
	}
	if(!isset($_SESSION['contador'])){
		$_SESSION['contador']=1;
		
	}else{
		$_SESSION['contador']++; 
	}
	//variables
	
	var_dump($_SESSION);
	echo "<br/>";
	var_dump($_REQUEST);
	echo "<br/>";
	var_dump($_COOKIE);
	echo "<br/>";
?>
<DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<title>Sessiones</title>
		<link rel="stylesheet" href="css/style.css">		
	</head>
	<body>
		<?php	
			
			echo (isset($_SESSION['nombre']) ?
				"<p>Hola, " . $_SESSION['nombre'] . "</p>":
				"<p>Hola Anonimo</p>");
				echo "Has estado aquí " . $_SESSION['contador'] ." veces";
		?>
		<form method="post" action="02_sesiones.php">
			<input type="text" name="nombre"/>
			<input type="submit" value="Enviar"/>		
		</form>
	</body>
</html>