<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	
	session_start();
	if (isset($_POST['nombre'])){
		$_SESSION['nombre'] = $_POST['nombre'];
	}
	if (isset($_POST['apellidos'])){
		$_SESSION['apellidos'] = $_POST['apellidos'];
	}
	if (isset($_POST['a1'])){
		header("Location:03_1_formularios3pasos.php");
	}
	if (isset($_POST['a2'])){
		header("Location:03_2_formularios3pasos.php");
	}
	if (isset($_POST['destroy'])){
	   session_destroy();
	   header("Location:03_1_formularios3pasos.php");
	}
?>
<DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<title>header</title>
		<link rel="stylesheet" href="css/style.css">		
	</head>
	<body>
	<?php
		if (isset($_POST['finalizar'])){
		?>
			<h1>Hola <?php echo $_SESSION['nombre'] . " " . $_SESSION['apellidos'];?></h1>
			</body>
			</html>
		<?php
		}
	?>